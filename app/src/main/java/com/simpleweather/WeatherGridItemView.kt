/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.core.widget.TextViewCompat

class WeatherGridItemView : LinearLayout {

    val unitView: TextView
    val valueView: TextView

    constructor (context: Context) : this(context, null)

    constructor(context: Context, @Nullable attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        LayoutInflater.from(context).inflate(R.layout.weather_grid_item, this, true)

        orientation = VERTICAL

        unitView = findViewById(R.id.unit)
        valueView = findViewById(R.id.value)
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.WeatherGridItemView, 0, 0)

        TextViewCompat.setTextAppearance(unitView, typedArray.getResourceId(R.styleable.WeatherGridItemView_unitTextAppearance, 0))
        unitView.text = typedArray.getString(R.styleable.WeatherGridItemView_unit)
        TextViewCompat.setTextAppearance(valueView, typedArray.getResourceId(R.styleable.WeatherGridItemView_valueTextAppearance, 0))
        valueView.text = typedArray.getString(R.styleable.WeatherGridItemView_value)
        val unitViewMargins = unitView.layoutParams as MarginLayoutParams
        unitViewMargins.bottomMargin = typedArray.getDimensionPixelSize(R.styleable.WeatherGridItemView_unitValueHorizontalMargin, 0)

        typedArray.recycle()
    }
}