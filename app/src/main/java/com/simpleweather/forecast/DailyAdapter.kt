/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.forecast

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.simpleweather.R
import com.simpleweather.WeatherDataGridView
import com.simpleweather.WeatherGridData
import com.simpleweather.response.Daily
import com.simpleweather.util.WeatherUtils
import com.revengeos.weathericons.WeatherIconsHelper
import rjsv.expframelayout.ExpandableFrameLayout

class DailyAdapter(context: Context) :
        RecyclerView.Adapter<DailyAdapter.ViewHolder>() {

    private var selectedItemPosition = -1
    private val weatherUtils: WeatherUtils = WeatherUtils(context)

    private val dataSet = mutableListOf<Daily>()
    private var timeZone = 0

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(private val view: View, private val adapter: DailyAdapter,
                     private val weatherUtils: WeatherUtils) : RecyclerView.ViewHolder(view) {
        private var isSelected = false

        fun bind(daily: Daily, timeZone : Int) {
            view.findViewById<TextView>(R.id.temperature).text = weatherUtils.getFormattedTemperature(daily.temp.day)
            view.findViewById<TextView>(R.id.day).text = weatherUtils.getWeekDayFromEpoch(daily.dt, timeZone).capitalize()
            view.findViewById<TextView>(R.id.extra_temp_data).text = "Min ${weatherUtils.getFormattedTemperature(daily.temp.min)} Max ${weatherUtils.getFormattedTemperature(daily.temp.max)}"

            val isDay = daily.weather[0].icon.takeLast(1) == "d"
            val state = WeatherIconsHelper.mapConditionIconToCode(daily.weather[0].id, isDay)

            view.findViewById<ImageView>(R.id.condition_icon).setImageResource(WeatherIconsHelper.getDrawable(state, view.context)!!)

            val dailyGridData = WeatherGridData(daily.sunrise, daily.sunset, timeZone,
                    daily.pressure, daily.humidity, daily.windDeg,
                    daily.windSpeed, null, null, null,
                    null, daily.pop, null, daily.dewPoint)
            view.findViewById<WeatherDataGridView>(R.id.daily_data).updateData(dailyGridData)

            val dailyDataContainer = view.findViewById<ExpandableFrameLayout>(R.id.daily_data_container)
            dailyDataContainer.animationDuration = 700

            val isItemSelected = adapterPosition == adapter.selectedItemPosition
            if (!isItemSelected && view.isSelected) {
                isSelected = false
                view.isSelected = isSelected
                dailyDataContainer.collapse()
            } else if (isItemSelected && !view.isSelected) {
                isSelected = true
                view.isSelected = isSelected
                dailyDataContainer.expand()
            }

            view.setOnClickListener { _ ->
                val previousSelectedItem : Int
                if (isItemSelected) {
                    previousSelectedItem = adapterPosition
                    adapter.selectedItemPosition = -1
                } else {
                    previousSelectedItem = adapter.selectedItemPosition
                    adapter.selectedItemPosition = adapterPosition
                }
                adapter.notifyItemChanged(previousSelectedItem)
                adapter.notifyItemChanged(adapterPosition)
            }
        }
    }

    fun setForecast(newDaily: List<Daily>, newTimeZone: Int) {
        timeZone = newTimeZone
        dataSet.clear()
        dataSet.addAll(newDaily)
        notifyDataSetChanged()
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.daily_forecast_item, viewGroup, false)

        return ViewHolder(view, this, weatherUtils)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(dataSet[position], timeZone)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
