/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.forecast

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revengeos.revengeui.widget.DisableScrollingLinearLayoutManager
import com.simpleweather.IconTextView
import com.simpleweather.R
import com.simpleweather.WeatherDataGridView
import com.simpleweather.WeatherGridData
import com.simpleweather.response.Hourly
import com.simpleweather.util.WeatherUtils
import com.revengeos.weathericons.WeatherIconsHelper
import rjsv.expframelayout.ExpandableFrameLayout

class HourlyAdapter(private val dataGridContainer: ExpandableFrameLayout,
                    private val dataGridView: WeatherDataGridView,
                    private val parentLayoutManager: DisableScrollingLinearLayoutManager) :
        RecyclerView.Adapter<HourlyAdapter.ViewHolder>() {

    private var selectedItemPosition = -1
    private val weatherUtils: WeatherUtils = WeatherUtils(dataGridContainer.context)

    private val dataSet = mutableListOf<Hourly>()
    private var timeShift = 0

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(private val view: View, private val adapter: HourlyAdapter,
                     private val dataGridContainer: ExpandableFrameLayout,
                     private val dataGridView: WeatherDataGridView,
                     private val parentLayoutManager: DisableScrollingLinearLayoutManager,
                     private val weatherUtils: WeatherUtils) : RecyclerView.ViewHolder(view) {
        private var isSelected = false

        fun bind(hourly: Hourly, timeShift: Int) {
            view.findViewById<TextView>(R.id.temperature).text = weatherUtils.getFormattedTemperature(hourly.temp)
            view.findViewById<TextView>(R.id.feels_like).text = weatherUtils.getFeelsLikeFormattedTemp(view.context, hourly.feelsLike)
            view.findViewById<TextView>(R.id.time).text = weatherUtils.getTimeFromEpoch(hourly.dt, timeShift)

            val isDay = hourly.weather[0].icon.takeLast(1) == "d"
            val state = WeatherIconsHelper.mapConditionIconToCode(hourly.weather[0].id, isDay)
            view.findViewById<ImageView>(R.id.condition_icon).setImageResource(WeatherIconsHelper.getDrawable(state, view.context)!!)

            view.findViewById<IconTextView>(R.id.hourly_humidity).textView.text = "${hourly.humidity} %"
            val windView = view.findViewById<IconTextView>(R.id.hourly_wind)
            windView.iconView.rotation = hourly.windDeg.toFloat()
            windView.textView.text = weatherUtils.getFormattedSpeed(hourly.windSpeed)

            val isItemSelected = adapterPosition == adapter.selectedItemPosition
            if (!isItemSelected && view.isSelected) {
                isSelected = false
                view.isSelected = isSelected
                if (adapter.selectedItemPosition == -1) {
                    dataGridContainer.collapse()
                    dataGridContainer.isSelected = false
                    parentLayoutManager.canScroll = true
                }
            } else if (isItemSelected && !view.isSelected) {
                isSelected = true
                view.isSelected = isSelected
                val gridData = WeatherGridData(null, null, timeShift, hourly.pressure,
                        hourly.humidity, null, null, hourly.visibility,
                        null, null, hourly.feelsLike, hourly.pop,
                        hourly.uvi, hourly.dewPoint)
                dataGridView.updateData(gridData)
                dataGridContainer.expand()
                dataGridContainer.isSelected = true
                parentLayoutManager.canScroll = false
            }
            view.setOnClickListener { _ ->
                val previousSelectedItem : Int
                if (isItemSelected) {
                    previousSelectedItem = adapterPosition
                    adapter.selectedItemPosition = -1
                } else {
                    previousSelectedItem = adapter.selectedItemPosition
                    adapter.selectedItemPosition = adapterPosition
                }
                adapter.notifyItemChanged(previousSelectedItem)
                adapter.notifyItemChanged(adapterPosition)
            }
        }
    }

    fun setForecast(newHourly: List<Hourly>, newTimeShift: Int) {
        timeShift = newTimeShift
        dataSet.clear()
        dataSet.addAll(newHourly)
        notifyDataSetChanged()
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.hourly_forecast_item, viewGroup, false)

        return ViewHolder(view, this, dataGridContainer, dataGridView, parentLayoutManager, weatherUtils)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(dataSet[position], timeShift)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
