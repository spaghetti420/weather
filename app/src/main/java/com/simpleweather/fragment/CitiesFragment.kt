/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.simpleweather.BuildConfig.DEBUG
import com.simpleweather.R
import com.simpleweather.database.entity.City
import com.simpleweather.database.model.AppDatabaseViewModel
import com.simpleweather.database.model.CitiesListViewModel
import com.simpleweather.util.SavedCityItemAnimator
import com.simpleweather.util.ViewTransitionUtils

class CitiesFragment : Fragment() {

    private val TAG = javaClass.toString()

    private lateinit var sharePrefs: SharedPreferences
    private val prefsListener = object : SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            savedCitiesAdapter.notifyDataSetChanged()
        }
    }

    private lateinit var savedCitiesAdapter: SavedCitiesViewAdapter
    private lateinit var searchViewContainer: View
    private lateinit var searchClose: Button

    private var listener: CityItemSelectedListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharePrefs = PreferenceManager.getDefaultSharedPreferences(context)
        sharePrefs.registerOnSharedPreferenceChangeListener(prefsListener)
        try {
            listener = context as CityItemSelectedListener
        } catch (e: ClassCastException) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_cities, container, false)

        searchViewContainer = v.findViewById(R.id.city_search_container)
        val citiesListContainer = v.findViewById<View>(R.id.cities_list_container)
        searchClose = v.findViewById(R.id.close_search)
        searchClose.setOnClickListener { _ ->
            ViewTransitionUtils.transitionViews(citiesListContainer, searchViewContainer)
        }

        val savedCitiesViewModel = ViewModelProvider(this).get(AppDatabaseViewModel::class.java)
        val savedCityItemListener = (object : SavedCitiesViewAdapter.SavedCityItemListener {
            override fun onItemDeleted(wId: Int) {
                listener?.onCityDeleted(wId)
                savedCitiesViewModel.removeSavedCity(wId)
            }

            override fun onItemSelected(wId: Int) {
                listener?.onCitySelected(wId)
            }
        })

        val keyboard: InputMethodManager = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val searchTextField = v.findViewById<EditText>(R.id.search_text_field)
        val toolbar = v.findViewById<Toolbar>(R.id.real_toolbar)
        toolbar.setOnMenuItemClickListener { it ->
            when (it.itemId) {
                R.id.add_city -> {
                    ViewTransitionUtils.transitionViews(searchViewContainer, citiesListContainer)
                    searchTextField.requestFocus()
                    keyboard.showSoftInput(searchTextField, InputMethodManager.SHOW_IMPLICIT)
                }
            }
            false
        }

        val citiesRecyclerView = v.findViewById<RecyclerView>(R.id.cities_list)
        val layoutManager = LinearLayoutManager(v.context)
        citiesRecyclerView.layoutManager = layoutManager

        savedCitiesAdapter = SavedCitiesViewAdapter(v.context, savedCityItemListener)

        citiesRecyclerView.adapter = savedCitiesAdapter
        savedCitiesAdapter.notifyDataSetChanged()
        citiesRecyclerView.itemAnimator = SavedCityItemAnimator()
        citiesRecyclerView.clipToOutline = true

        savedCitiesViewModel.weatherAndCity.observe(viewLifecycleOwner, {
            if (DEBUG && it.isNotEmpty()) {
                Log.d(TAG, "${it.last().name} has been added to saved cities")
            }
            savedCitiesAdapter.setCitiesList(it)
        })


        val searchItemListener = (object : SearchViewAdapter.SearchItemListener {
            override fun onItemSelected(city: City) {
                if (DEBUG) Log.d(TAG, "Item selected")
                savedCitiesViewModel.addSavedCity(city)
                ViewTransitionUtils.transitionViews(citiesListContainer, searchViewContainer)
            }

        })

        val myRecyclerView = v.findViewById<RecyclerView>(R.id.search_list)
        val searchViewAdapter = SearchViewAdapter(v.context, searchItemListener)
        myRecyclerView.adapter = searchViewAdapter
        myRecyclerView.layoutManager = LinearLayoutManager(v.context)

        val citiesListViewModel = ViewModelProvider(this).get(CitiesListViewModel::class.java)
        citiesListViewModel.citiesList.observe(viewLifecycleOwner, {
            searchViewAdapter.setCitiesList(it)
        })

        searchTextField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                citiesListViewModel.searchNameChanged(s.toString())
            }

        })
        searchTextField.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                searchViewAdapter.setCitiesList(emptyList())
                searchTextField.text.clear()
                keyboard.hideSoftInputFromWindow(searchTextField.windowToken, 0)
            }
        }
        searchTextField.setupClearButtonWithAction()

        val collapsingToolbar = v.findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar_layout)
        ViewCompat.setOnApplyWindowInsetsListener(collapsingToolbar, null)

        ViewCompat.setOnApplyWindowInsetsListener(v) { _, inset ->
            val topInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).top
            val leftInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).left +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).left
            val rightInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).right +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).right

            citiesListContainer.setPadding(leftInset, topInset, rightInset, 0)
            searchViewContainer.setPadding(leftInset, topInset, rightInset, 0)

            return@setOnApplyWindowInsetsListener inset
        }
        return v
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (!menuVisible) {
            if (this::savedCitiesAdapter.isInitialized) {
                savedCitiesAdapter.clearSelection()
            }
            if (this::searchViewContainer.isInitialized && this::searchClose.isInitialized &&
                    searchViewContainer.visibility == View.VISIBLE) {
                this.searchClose.performClick()
            }
        }
    }

    interface CityItemSelectedListener {
        fun onCitySelected(wId: Int)
        fun onCityDeleted(wId: Int)
    }

    private fun EditText.setupClearButtonWithAction() {
        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val clearIcon = if (editable?.isNotEmpty() == true) R.drawable.ic_clear else 0
                setCompoundDrawablesWithIntrinsicBounds(0, 0, clearIcon, 0)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                    this.text.clear()
                    return@OnTouchListener true
                }
            }
            return@OnTouchListener false
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                CitiesFragment().apply {
                }
    }
}