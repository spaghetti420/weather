/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.simpleweather.R
import com.simpleweather.database.entity.City


class SearchViewAdapter(val context: Context,
                        val searchItemListener: SearchItemListener)
    : RecyclerView.Adapter<SearchViewAdapter.ViewHolder>() {

    private var cities = listOf<City?>()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(private val view: View,
                     val searchItemListener: SearchItemListener) : RecyclerView.ViewHolder(view) {
        fun bind(city: City) {
            val cityName = view.findViewById<TextView>(R.id.name)

            val text = """${city.name}, ${city.country}"""
            cityName.text = text

            view.setOnClickListener {
                searchItemListener.onItemSelected(city)
            }
        }
    }

    fun setCitiesList(cities: List<City?>) {
        this.cities = cities
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.search_city_item, parent, false)
        return ViewHolder(view, searchItemListener)
    }


    override fun getItemCount(): Int {
        return cities.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        cities[position]?.let { holder.bind(it) }
    }

    interface SearchItemListener {
        fun onItemSelected(city: City)
    }
}