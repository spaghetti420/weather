/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.fragment

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.preference.*
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.revengeos.revengeui.recyclerview.BounceEdgeEffectFactory
import com.simpleweather.R
import com.simpleweather.credits.CreditsDialogFragment

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    private val TAG = javaClass.toString()
    private lateinit var sharedPrefs: SharedPreferences
    private var customisationCategory: PreferenceCategory? = null
    private var appBackgroundPreference: ListPreference? = null
    private var imagePickerPreference: Preference? = null
    private var creditsPreference: Preference? = null
    private lateinit var listener: CustomImageListener

    private lateinit var creditsDialogFragment: CreditsDialogFragment

    var pickerLauncher = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
        if (uri != null) {
            listener.onCustomImageSelected(uri)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        try {
            listener = context as CustomImageListener
        } catch (e: ClassCastException) {
        }

        creditsDialogFragment = CreditsDialogFragment.newInstance()
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.user_preferences, rootKey)
    }

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)

        val collapsingToolbar = v.findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar_layout)
        ViewCompat.setOnApplyWindowInsetsListener(collapsingToolbar, null)

        ViewCompat.setOnApplyWindowInsetsListener(v) { _, inset ->
            val topInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).top
            val leftInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).left +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).left
            val rightInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).right +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).right

            v.setPadding(leftInset, topInset, rightInset, 0)

            return@setOnApplyWindowInsetsListener inset
        }

        listView.edgeEffectFactory = BounceEdgeEffectFactory()
    }

    override fun onStart() {
        super.onStart()
        customisationCategory = preferenceScreen.findPreference(CUSTOMISATION_CATEGORY_KEY)
        imagePickerPreference = preferenceScreen.findPreference(IMAGE_PICKER_PREFERENCE)
        appBackgroundPreference = preferenceScreen.findPreference(APP_BACKGROUND_SETTING_KEY)
        appBackgroundPreference?.let {
            sharedPrefs.registerOnSharedPreferenceChangeListener(this)
            updateImagePickerPreferenceVisibility(it.value == "1")
        }
        creditsPreference = preferenceScreen.findPreference(CREDITS_PREFERENCE)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        if (preference != null) {
            when (preference) {
                imagePickerPreference -> pickerLauncher.launch("image/*")
                creditsPreference -> creditsDialogFragment.show(childFragmentManager, "")
            }
        }
        return super.onPreferenceTreeClick(preference)
    }

    private fun updateImagePickerPreferenceVisibility(visible: Boolean) {
        if (customisationCategory != null && imagePickerPreference != null) {
            if (visible) {
                customisationCategory!!.addPreference(imagePickerPreference)
            } else {
                customisationCategory!!.removePreference(imagePickerPreference)
            }
        }
    }

    companion object {
        const val MEASUREMENT_UNITS_KEY = "measurement_units"
        const val CUSTOMISATION_CATEGORY_KEY = "customisation"
        const val APP_BACKGROUND_SETTING_KEY = "app_background_setting_key"
        const val IMAGE_PICKER_PREFERENCE = "image_picker"
        const val CURRENT_LOCATION_WEATHER_KEY = "current_location_weather"
        const val CREDITS_PREFERENCE = "credits"

        @JvmStatic
        fun newInstance() =
                SettingsFragment().apply {
                }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            APP_BACKGROUND_SETTING_KEY -> {
                appBackgroundPreference?.let {
                    updateImagePickerPreferenceVisibility(it.value == "1")
                }
            }
        }
    }

    interface CustomImageListener {
        fun onCustomImageSelected(uri: Uri)
    }
}