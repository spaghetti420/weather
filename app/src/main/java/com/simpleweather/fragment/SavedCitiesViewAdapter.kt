/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.SwipeRevealLayout
import com.simpleweather.R
import com.simpleweather.database.entity.WeatherAndCity
import com.simpleweather.util.WeatherUtils
import com.revengeos.weathericons.WeatherIconsHelper


class SavedCitiesViewAdapter(val context: Context, private val savedCityItemListener: SavedCityItemListener) : RecyclerView.Adapter<SavedCitiesViewAdapter.ViewHolder>() {

    private val weatherUtils: WeatherUtils = WeatherUtils(context)
    private var selectedPosition = -1

    private val itemDiffCallback = object : DiffUtil.ItemCallback<WeatherAndCity>() {
        override fun areContentsTheSame(oldItem: WeatherAndCity, newItem: WeatherAndCity): Boolean {
            return (oldItem.current.dt == newItem.current.dt)
        }

        override fun areItemsTheSame(oldItem: WeatherAndCity, newItem: WeatherAndCity): Boolean {
            return (oldItem.wId == newItem.wId)
        }
    }
    private val cities = AsyncListDiffer(this, itemDiffCallback)

    inner class ViewHolder(private val view: View, private val savedCityItemListener: SavedCityItemListener,
                           private val weatherUtils: WeatherUtils) : RecyclerView.ViewHolder(view) {
        private val foreground = view.findViewById<View>(R.id.foreground)
        private var selected = false
        init {
            if (view is SwipeRevealLayout) {
                view.setLockDrag(true)
            }
        }

        fun bind(weatherAndCity: WeatherAndCity, newSelected: Boolean) {
            val cityName = view.findViewById<TextView>(R.id.name)
            val temp = view.findViewById<TextView>(R.id.temperature)
            val feelsLike = view.findViewById<TextView>(R.id.feels_like)
            val text = """${weatherAndCity.name}, ${weatherAndCity.country}"""
            cityName.text = text

            temp.text = weatherUtils.getFormattedTemperature(weatherAndCity.current.temp)
            feelsLike.text = weatherUtils.getFeelsLikeFormattedTemp(view.context, weatherAndCity.current.feelsLike)

            val icon = view.findViewById<ImageView>(R.id.current_icon)

            val isDay = weatherAndCity.current.weather[0].icon.takeLast(1) == "d"
            val state = WeatherIconsHelper.mapConditionIconToCode(weatherAndCity.current.weather[0].id, isDay)
            WeatherIconsHelper.getDrawable(state, view.context)?.let {
                icon.setImageResource(it)
            }
            val deleteButton = view.findViewById<View>(R.id.remove_city)
            foreground.setOnClickListener {
                savedCityItemListener.onItemSelected(weatherAndCity.wId)
            }
            if (weatherAndCity.wId == 0) {
                foreground.setOnLongClickListener(null)
            } else {
                foreground.setOnLongClickListener {
                    if (selected) {
                        selectedPosition = -1
                        notifyItemChanged(adapterPosition)
                    } else {
                        notifyItemChanged(selectedPosition)
                        selectedPosition = adapterPosition
                        notifyItemChanged(adapterPosition)
                    }

                    true
                }
            }

            selected = newSelected

            deleteButton.setOnClickListener {
                selectedPosition = -1
                savedCityItemListener.onItemDeleted(weatherAndCity.wId)
            }
        }

        fun isSelected(): Boolean {
            return selected
        }
    }

    fun clearSelection() {
        if (selectedPosition != -1) {
            val oldPosition = selectedPosition
            selectedPosition = -1
            notifyItemChanged(oldPosition)
        }
    }

    fun setCitiesList(newList: List<WeatherAndCity>) {
        cities.submitList(newList)
    }

    override fun getItemId(position: Int): Long {
        return cities.currentList[position].wId.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.saved_city_item, parent, false)
        return ViewHolder(view, savedCityItemListener, weatherUtils)
    }


    override fun getItemCount(): Int {
        return cities.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val city = cities.currentList[position]
        holder.bind(city, selectedPosition == position)
    }

    interface SavedCityItemListener {
        fun onItemDeleted(wId: Int)
        fun onItemSelected(wId: Int)
    }

    companion object {
        const val SELECTION_CHANGED = 69420
    }
}