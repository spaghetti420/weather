/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.fragment

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.revengeos.revengeui.utils.ReflectionUtils
import com.revengeos.revengeui.widget.DisableScrollingLinearLayoutManager
import com.revengeos.weathericons.WeatherIconsHelper
import com.simpleweather.*
import com.simpleweather.database.entity.City
import com.simpleweather.database.entity.WeatherAndCity
import com.simpleweather.database.model.AppDatabaseViewModel
import com.simpleweather.database.model.CitiesListViewModel
import com.simpleweather.forecast.DailyAdapter
import com.simpleweather.forecast.HourlyAdapter
import com.simpleweather.response.Hourly
import com.simpleweather.response.OneCallResponse
import com.simpleweather.util.WeatherUtils
import com.yayandroid.locationmanager.base.LocationBaseFragment
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration
import com.yayandroid.locationmanager.configuration.GooglePlayServicesConfiguration
import com.yayandroid.locationmanager.configuration.LocationConfiguration
import com.yayandroid.locationmanager.configuration.PermissionConfiguration
import com.yayandroid.locationmanager.constants.FailType
import com.yayandroid.locationmanager.constants.ProviderType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import rjsv.expframelayout.ExpandableFrameLayout


open class DayWeatherFragment : LocationBaseFragment(), WeatherDataService.WeatherDataListener {

    private enum class FragmentState {
        LOADING,
        ERROR,
        SHOW_WEATHER,
    }

    private lateinit var swipeRefresh: SwipeRefreshLayout

    private lateinit var currentTemp: TextView
    private lateinit var currentTempEnd: TextView
    private lateinit var currentLocationView: TextView
    private lateinit var currentLocationEndView: TextView
    private lateinit var currentTempFeelsLike: TextView
    private lateinit var currentTempFeelsLikeEnd: TextView
    private lateinit var currentIcon: ImageView

    private lateinit var currentData: WeatherDataGridView

    private lateinit var offlineModeIndicator : IconTextView

    private lateinit var todayForecast: RecyclerView
    private lateinit var todayForecastAdapter: HourlyAdapter
    private lateinit var todayForecastForecastLayoutManager : DisableScrollingLinearLayoutManager
    private lateinit var todayForecastHourlyDataContainer: ExpandableFrameLayout
    private lateinit var todayForecastHourlyDataView: WeatherDataGridView
    private lateinit var nextDaysForecast : RecyclerView
    private lateinit var nextDaysForecastAdapter : DailyAdapter
    private lateinit var errorContainerView : View

    private var weatherDataAvailable = false

    private lateinit var tomorrowTemp : TextView
    private lateinit var tomorrowFeelsLike: TextView
    private lateinit var tomorrowIcon : ImageView
    private lateinit var tomorrowData : WeatherDataGridView
    private lateinit var tomorrowForecast: RecyclerView
    private lateinit var tomorrowForecastAdapter: HourlyAdapter
    private lateinit var tomorrowForecastLayoutManager : DisableScrollingLinearLayoutManager
    private lateinit var tomorrowForecastHourlyDataContainer: ExpandableFrameLayout
    private lateinit var tomorrowForecastHourlyDataView: WeatherDataGridView

    private lateinit var progressBarContainer: View
    private lateinit var nextHoursTextView: View
    private lateinit var todayForecastContainer: View
    private lateinit var tomorrowTextView: View
    private lateinit var tomorrowForecastContainer: View

    private lateinit var savedCitiesViewModel: AppDatabaseViewModel
    private lateinit var weatherLiveData: LiveData<WeatherAndCity>
    private var wIdToListen = 0
    private var currentLocation: Location? = null
    private var nextUpdate = System.currentTimeMillis() / 1000L + 900

    // Determine whenever to update data from server
    private var forceUpdateData = true

    private val weatherLiveDataListener = Observer<WeatherAndCity> { it ->
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            val current = it?.current
            if (current != null) {
                val today = it.daily!![0]

                val todayWeatherHeaderData = WeatherHeaderData(current.temp, current.feelsLike,
                        current.weather[0].icon, current.weather[0].id)
                val todayWeatherGridData = WeatherGridData(current.sunrise, current.sunset,
                        it.timezone_offset, current.pressure, current.humidity, current.windDeg,
                        current.windSpeed, current.visibility, today.temp.min, today.temp.max, null,
                        null, null, null)

                // Update today's hourly forecast data
                var todayHourlyForecast = (it.hourly)!!.subList(0, 25).toMutableList()
                if (todayHourlyForecast[0].dt < current.dt) {
                    // Remove first element since it's time is earlier than current weather
                    todayHourlyForecast.removeAt(0)
                } else {
                    todayHourlyForecast.removeLast()
                }

                // Update tomorrow's hourly forecast data
                val tomorrow = it.daily!![1]

                val tomorrowWeatherHeaderData = WeatherHeaderData(tomorrow.temp.day, tomorrow.feels_like.day,
                        tomorrow.weather[0].icon, tomorrow.weather[0].id)

                val tomorrowWeatherGridData = WeatherGridData(tomorrow.sunrise, tomorrow.sunset, it.timezone_offset,
                        tomorrow.pressure, tomorrow.humidity, tomorrow.windDeg,
                        tomorrow.windSpeed, null, tomorrow.temp.min, tomorrow.temp.max,
                        null, tomorrow.pop, null, null)

                val tomorrowHourlyForecast = mutableListOf<Hourly>()

                // Filter all the items oneCallResponse.hourly that have the same day of tomorrow
                for (hourlyItem in it.hourly!!) {
                    if (weatherUtils.getDateFromEpoch(hourlyItem.dt, it.timezone_offset)
                            == weatherUtils.getDateFromEpoch(tomorrow.dt, it.timezone_offset)) {
                        tomorrowHourlyForecast.add(hourlyItem)
                    }
                }

                // Remove first two days of list of days since they are already displayed above
                val nextDaysForecast = it.daily!!.subList(2, it.daily!!.size)

                val weatherFragmentData = DayWeatherFragmentData(todayWeatherHeaderData,
                        todayWeatherGridData, todayHourlyForecast, tomorrowWeatherHeaderData, tomorrowWeatherGridData, tomorrowHourlyForecast, nextDaysForecast)

                withContext(Dispatchers.Main) {
                    setLocation(it.name)
                    //  dayWeatherFragment.setOfflineMode(cached)
                    setFragmentData(weatherFragmentData)
                }
            }
        }
    }

    private lateinit var weatherUtils: WeatherUtils
    private lateinit var weatherFragmentData: DayWeatherFragmentData

    private lateinit var sharePrefs: SharedPreferences
    private val prefsListener = object : SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            when (key) {
                SettingsFragment.MEASUREMENT_UNITS_KEY -> updateWeatherViews()
                SettingsFragment.CURRENT_LOCATION_WEATHER_KEY -> {
                    if (!sharePrefs.getBoolean(SettingsFragment.CURRENT_LOCATION_WEATHER_KEY, true)) {
                        currentLocation = null
                    }
                    getLocationIfNecessary()
                }
            }
        }
    }

    private var fragmentListener: DayWeatherFragmentListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        weatherUtils = WeatherUtils(context)
        sharePrefs = PreferenceManager.getDefaultSharedPreferences(context)
        sharePrefs.registerOnSharedPreferenceChangeListener(prefsListener)

        try {
            fragmentListener = context as DayWeatherFragmentListener
        } catch (e: ClassCastException) {
        }
    }

    override fun onResume() {
        super.onResume()
        val currentTime = System.currentTimeMillis() / 1000L
        if (currentTime >= nextUpdate) {
            nextUpdate = System.currentTimeMillis() / 1000L + 900
            forceUpdateData = true
            getLocationIfNecessary()
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_day_weather, container, false)

        swipeRefresh = v.findViewById(R.id.swipe_refresh)
        // Prevent user from refreshing when data is not available yet
        swipeRefresh.isEnabled = false
        swipeRefresh.setOnRefreshListener {
            // Request new location
            forceUpdateData = true
            getLocationIfNecessary()
        }

        currentTemp = v.findViewById(R.id.current_temperature)
        currentTempEnd = v.findViewById(R.id.current_temperature_end)
        currentLocationView = v.findViewById(R.id.current_location)
        currentLocationEndView = v.findViewById(R.id.current_location_end)
        currentTempFeelsLike = v.findViewById(R.id.current_temp_feels_like)
        currentTempFeelsLikeEnd = v.findViewById(R.id.current_temp_feels_like_end)
        currentIcon = v.findViewById(R.id.current_icon)

        currentData = v.findViewById(R.id.current_data)

        errorContainerView = v.findViewById(R.id.error_container)
        val errorActioBtn = v.findViewById<View>(R.id.error_action_btn)
        errorActioBtn.setOnClickListener {
            fragmentListener?.goToCitiesFragment()
        }

        todayForecast = v.findViewById(R.id.today_forecast)
        (todayForecast.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        todayForecastForecastLayoutManager = DisableScrollingLinearLayoutManager(v.context, LinearLayoutManager.HORIZONTAL, false)
        todayForecastHourlyDataContainer = v.findViewById(R.id.today_hourly_data_container)
        todayForecastHourlyDataContainer.animationDuration = 700
        todayForecastHourlyDataView = v.findViewById(R.id.today_hourly_data)
        todayForecastAdapter = HourlyAdapter(todayForecastHourlyDataContainer, todayForecastHourlyDataView, todayForecastForecastLayoutManager)
        todayForecast.adapter = todayForecastAdapter
        todayForecast.layoutManager = todayForecastForecastLayoutManager

        nextDaysForecast = v.findViewById(R.id.next_days_forecast)
        (nextDaysForecast.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        nextDaysForecastAdapter = DailyAdapter(v.context)
        nextDaysForecast.adapter = nextDaysForecastAdapter
        nextDaysForecast.layoutManager = LinearLayoutManager(v.context)

        ViewCompat.setOnApplyWindowInsetsListener(v) { _, inset ->
            val topInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).top
            val topInsetView = v.findViewById<View>(R.id.top_inset)
            topInsetView.layoutParams.height = topInset

            swipeRefresh.setProgressViewOffset(false, topInset, topInset + 50)

            val leftInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).left +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).left
            val rightInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).right +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).right

            val scrollContainer = v.findViewById<NestedScrollView>(R.id.scroll_container)
            scrollContainer.setPadding(leftInset, 0, rightInset, 0)

            return@setOnApplyWindowInsetsListener inset
        }

        offlineModeIndicator = v.findViewById(R.id.offline_mode)

        tomorrowTemp = v.findViewById(R.id.tomorrow_temperature)
        tomorrowFeelsLike = v.findViewById(R.id.tomorrow_feels_like)
        tomorrowIcon = v.findViewById(R.id.tomorrow_icon)
        tomorrowData = v.findViewById(R.id.tomorrow_data)
        tomorrowForecast = v.findViewById(R.id.tomorrow_forecast)
        (tomorrowForecast.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        tomorrowForecastLayoutManager = DisableScrollingLinearLayoutManager(v.context, LinearLayoutManager.HORIZONTAL, false)
        tomorrowForecastHourlyDataContainer = v.findViewById(R.id.tomorrow_hourly_data_container)
        tomorrowForecastHourlyDataContainer.animationDuration = 700
        tomorrowForecastHourlyDataView = v.findViewById(R.id.tomorrow_hourly_data)
        tomorrowForecastAdapter = HourlyAdapter(tomorrowForecastHourlyDataContainer, tomorrowForecastHourlyDataView, tomorrowForecastLayoutManager)
        tomorrowForecast.adapter = tomorrowForecastAdapter
        tomorrowForecast.layoutManager = tomorrowForecastLayoutManager

        progressBarContainer = v.findViewById(R.id.progress_bar_container)
        nextHoursTextView = v.findViewById(R.id.next_hours)
        todayForecastContainer = v.findViewById(R.id.today_forecast_container)
        tomorrowTextView = v.findViewById(R.id.tomorrow)
        tomorrowForecastContainer = v.findViewById(R.id.tomorrow_forecast_container)

        todayForecastContainer.clipToOutline = true
        tomorrowForecastContainer.clipToOutline = true
        nextDaysForecast.clipToOutline = true

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedCitiesViewModel = ViewModelProvider(this).get(AppDatabaseViewModel::class.java)
        savedCitiesViewModel.getWeatherDataService().addListener(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("wId", wIdToListen)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            wIdToListen = savedInstanceState.getInt("wId")
        }
        lifecycleScope.launch {
            weatherLiveData = savedCitiesViewModel.getWeather(wIdToListen)
            weatherLiveData.observe(viewLifecycleOwner, weatherLiveDataListener)
        }
        getLocationIfNecessary()
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onDetach() {
        super.onDetach()
        savedCitiesViewModel.getWeatherDataService().removeListener(this)
        fragmentListener = null
    }

    // This runs only once on app launch
    override fun onLocationChanged(loc: Location?) {
        handleNewLocation(loc)
    }

    override fun onLocationFailed(type: Int) {
        if (type == FailType.PERMISSION_DENIED && !sharePrefs.getBoolean(USER_REFUSED_LOCATION_PERMISSION_KEY, false)) {
            sharePrefs.edit().putBoolean(USER_REFUSED_LOCATION_PERMISSION_KEY, true).apply()
        }
        handleNewLocation(null)
    }

    private fun getLocationIfNecessary() {
        if (sharePrefs.getBoolean(SettingsFragment.CURRENT_LOCATION_WEATHER_KEY, true)) {
            getLocation()
        } else {
            handleNewLocation(null)
        }
    }

    private fun handleNewLocation(loc: Location?) {
        if (loc != null) {
            currentLocation = loc
            getWeatherForCurrentLocation(loc)
        } else {
            currentLocation?.let {
                getWeatherForCurrentLocation(it)
            } ?: run {
                savedCitiesViewModel.removeSavedCity(0)
                savedCitiesViewModel.weatherAndCity.observe(viewLifecycleOwner, object : Observer<List<WeatherAndCity>> {
                    override fun onChanged(list: List<WeatherAndCity>?) {
                        // Try to get the first saved city with id != 0
                        if (list != null && list.isNotEmpty()) {
                            for (city in list) {
                                if (city.wId != 0) {
                                    savedCitiesViewModel.weatherAndCity.removeObserver(this)
                                    changeWIdToListen(city.wId)
                                    // We only listen to cities once to get the first city with Id not 0
                                    updateAllData()
                                    return
                                }
                            }
                            // If nothing can be found, show error
                        } else {
                            updateFragmentState(FragmentState.ERROR)
                        }
                    }
                })
            }
        }
    }

    override fun getLocationConfiguration(): LocationConfiguration {
        val locationConfig = LocationConfiguration.Builder()
                .keepTracking(false)
                .useGooglePlayServices(GooglePlayServicesConfiguration.Builder().build())
                .useDefaultProviders(DefaultProviderConfiguration.Builder()
                        .setWaitPeriod(ProviderType.GPS, 5 * 1000)
                        .acceptableAccuracy(150f)
                        .setWaitPeriod(ProviderType.NETWORK, 5 * 1000).build())
        if (!sharePrefs.getBoolean(USER_REFUSED_LOCATION_PERMISSION_KEY, false)) {
            locationConfig.askForPermission(PermissionConfiguration.Builder().build())
        }
        return locationConfig.build()
    }

    override fun onOneCallWeatherUpdateSuccess(oneCallResponse: OneCallResponse, cached: Boolean, city: City) {
        setOfflineMode(false)
    }

    override fun onOneCallWeatherUpdateFailed(errorMessage: String) {
        setOfflineMode(true)
    }

    // false to set visibility gone
    // true to set visibility visible
    private fun updateFragmentState(newState: FragmentState) {
        val contentFinalVisibility = when (newState) {
            FragmentState.LOADING -> View.GONE
            FragmentState.ERROR -> View.GONE
            FragmentState.SHOW_WEATHER -> View.VISIBLE
        }
        val progressBarFinalAlpha = when (newState) {
            FragmentState.LOADING -> 1f
            FragmentState.ERROR -> 0f
            FragmentState.SHOW_WEATHER -> 0f
        }
        val errorFinalVisibility = when (newState) {
            FragmentState.LOADING -> View.GONE
            FragmentState.ERROR -> View.VISIBLE
            FragmentState.SHOW_WEATHER -> View.GONE
        }
        if (progressBarContainer.alpha != progressBarFinalAlpha) {
            progressBarContainer.animate().alpha(progressBarFinalAlpha).setDuration(200).withStartAction {
                if (newState == FragmentState.LOADING) {
                    progressBarContainer.visibility = View.VISIBLE
                }
            }.withEndAction {
                if (newState != FragmentState.LOADING) {
                    progressBarContainer.visibility = View.GONE
                }
                setWeatherViewsVisibility(contentFinalVisibility)
                errorContainerView.visibility = errorFinalVisibility
            }
        } else {
            if (newState == FragmentState.LOADING) {
                progressBarContainer.visibility = View.VISIBLE
            } else {
                progressBarContainer.visibility = View.GONE
            }
            setWeatherViewsVisibility(contentFinalVisibility)
            errorContainerView.visibility = errorFinalVisibility
        }
    }

    private fun setWeatherViewsVisibility(visibility: Int) {
        currentData.visibility = visibility
        nextHoursTextView.visibility = visibility
        todayForecastContainer.visibility = visibility
        tomorrowTextView.visibility = visibility
        tomorrowForecastContainer.visibility = visibility
        nextDaysForecast.visibility = visibility
    }

    /*private fun setupAppearAnimation(view: View) {
        if (view.visibility == View.GONE) {
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            view.alpha = 0f
            view.scaleX = 0.75f
            view.scaleY = 0.75f
            view.visibility = View.VISIBLE
        }
    }*/

    private fun getWeatherForCurrentLocation(location: Location) {
        val citiesListViewModel = ViewModelProvider(this).get(CitiesListViewModel::class.java)
        val closestCity = citiesListViewModel.getNearestCity(location.latitude.toFloat(), location.longitude.toFloat())
        val city = City(0, closestCity!!.country, closestCity.name, location.latitude.toFloat(), location.longitude.toFloat())
        savedCitiesViewModel.addSavedCity(city)
        updateAllData()
    }

    private fun updateAllData() {
        if (forceUpdateData) {
            savedCitiesViewModel.updateAll()
            forceUpdateData = false
        }
    }

    fun changeWIdToListen(newWId: Int) {
        if (wIdToListen != newWId) {
            wIdToListen = newWId

            weatherLiveData.removeObservers(viewLifecycleOwner)
            lifecycleScope.launch {
                weatherLiveData = savedCitiesViewModel.getWeather(newWId)
                weatherLiveData.observe(viewLifecycleOwner, weatherLiveDataListener)
            }
        }
    }

    private fun setOfflineMode(value: Boolean) {
        offlineModeIndicator.alpha = if (value) 1f else 0f
    }

    private fun setFragmentData(dayWeatherFragmentData: DayWeatherFragmentData) {
        weatherFragmentData = dayWeatherFragmentData
        fragmentListener?.onBackgroundChanged(weatherUtils.getBackgroundResId(dayWeatherFragmentData.todayWeatherHeaderData.weatherId))
        updateWeatherViews()
    }

    private fun updateWeatherViews() {
        if (this::weatherFragmentData.isInitialized && view != null) {
            setTodayWeatherHeaderData(weatherFragmentData.todayWeatherHeaderData)
            setTodayWeatherDataGrid(weatherFragmentData.todayWeatherGridData)
            todayForecastAdapter.setForecast(weatherFragmentData.todayHourlyForecast, weatherFragmentData.todayWeatherGridData.timeZone)

            setTomorrowWeatherHeaderData(weatherFragmentData.tomorrowWeatherHeaderData)
            setTomorrowWeatherDataGrid(weatherFragmentData.tomorrowWeatherGridData)
            tomorrowForecastAdapter.setForecast(weatherFragmentData.tomorrowHourlyForecast, weatherFragmentData.tomorrowWeatherGridData.timeZone)

            nextDaysForecastAdapter.setForecast(weatherFragmentData.nextDaysForecast, weatherFragmentData.todayWeatherGridData.timeZone)
            updateFragmentState(FragmentState.SHOW_WEATHER)
            swipeRefresh.isEnabled = true
            swipeRefresh.isRefreshing = false
        }
    }

    private fun setLocation(name: String) {
        currentLocationView.text = name
        currentLocationEndView.text = name
    }

    private fun setTodayWeatherHeaderData(weatherHeaderData: WeatherHeaderData) {
        val temperature = weatherUtils.getFormattedTemperature(weatherHeaderData.temp)
        currentTemp.text = temperature
        currentTempEnd.text = temperature
        val feelsLikeText = weatherUtils.getFeelsLikeFormattedTemp(requireContext(), weatherHeaderData.tempFeelsLike)
        currentTempFeelsLike.text = feelsLikeText
        currentTempFeelsLikeEnd.text = feelsLikeText

        val isDay = weatherHeaderData.weatherIcon.takeLast(1) == "d"
        val state = WeatherIconsHelper.mapConditionIconToCode(weatherHeaderData.weatherId, isDay)
        currentIcon.setImageResource(WeatherIconsHelper.getDrawable(state, requireContext())!!)
    }

    private fun setTodayWeatherDataGrid(weatherGridData: WeatherGridData) {
        currentData.updateData(weatherGridData)
    }

    private fun setTomorrowWeatherHeaderData(weatherHeaderData: WeatherHeaderData) {
        val temperature = weatherUtils.getFormattedTemperature(weatherHeaderData.temp)
        tomorrowTemp.text = temperature
        val feelsLikeText = weatherUtils.getFeelsLikeFormattedTemp(requireContext(), weatherHeaderData.tempFeelsLike)
        tomorrowFeelsLike.text = feelsLikeText

        val isDay = weatherHeaderData.weatherIcon.takeLast(1) == "d"
        val state = WeatherIconsHelper.mapConditionIconToCode(weatherHeaderData.weatherId, isDay)
        tomorrowIcon.setImageResource(WeatherIconsHelper.getDrawable(state, requireContext())!!)
    }

    private fun setTomorrowWeatherDataGrid(weatherGridData: WeatherGridData) {
        tomorrowData.updateData(weatherGridData)
    }

    private fun setHourlyForecastAdapter(hourlyAdapter: HourlyAdapter) {
        if (!weatherDataAvailable) {
            weatherDataAvailable = true
        }
        todayForecast.adapter = hourlyAdapter
    }

    interface DayWeatherFragmentListener {
        fun onBackgroundChanged(drawableId: Int)
        fun goToCitiesFragment()
    }

    companion object {
        const val USER_REFUSED_LOCATION_PERMISSION_KEY = "user_refused_loc_perm"
        @JvmStatic
        fun newInstance() =
                DayWeatherFragment().apply {
                }
    }
}