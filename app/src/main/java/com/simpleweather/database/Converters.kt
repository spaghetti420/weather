/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.simpleweather.response.Current
import com.simpleweather.response.Daily
import com.simpleweather.response.Hourly

class Converters {
    @TypeConverter
    fun fromStringToCurrent(value: String?): Current {
        val listType = object : TypeToken<Current?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromCurrentToString(current: Current): String {
        val gson = Gson()
        return gson.toJson(current)
    }

    @TypeConverter
    fun fromStringToHourly(value: String?): MutableList<Hourly> {
        val listType = object : TypeToken<MutableList<Hourly>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromHourlyArrayListToString(list: MutableList<Hourly>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromStringToDaily(value: String?): MutableList<Daily> {
        val listType = object : TypeToken<MutableList<Daily>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromDailyArrayListToString(list: MutableList<Daily>): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}