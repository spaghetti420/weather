/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.simpleweather.database.CityDatabase
import com.simpleweather.database.dao.CitiesListDAO
import com.simpleweather.database.entity.City

class CitiesListRepository {
    private val citiesListDAO: CitiesListDAO

    constructor(application: Application) {
        citiesListDAO = CityDatabase.getInstance(application).citiesListDao()
    }

    fun findAllCities(): LiveData<List<City>> {
        return citiesListDAO.findAllCities()
    }

    fun searchByCityName(name: String): LiveData<List<City>> {
        return citiesListDAO.searchByCityName(name)
    }

    fun getNearestCity(lat: Float, lon: Float): City {
        return citiesListDAO.getNearestCity(lat, lon)
    }
}