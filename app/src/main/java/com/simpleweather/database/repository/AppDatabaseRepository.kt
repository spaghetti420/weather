/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.simpleweather.database.AppDatabase
import com.simpleweather.database.dao.AppDatabaseDAO
import com.simpleweather.database.entity.City
import com.simpleweather.database.entity.Weather
import com.simpleweather.database.entity.WeatherAndCity

class AppDatabaseRepository(application: Application) {
    private val appDatabaseDAO: AppDatabaseDAO = AppDatabase.getInstance(application).SavedCitiesDAO()

    fun getAllSavedCities(): List<City> {
        return appDatabaseDAO.getCities()
    }

    fun removeSavedCity(cId: Int) {
        appDatabaseDAO.removeCity(cId)
    }

    fun addWeather(weather: Weather) {
        appDatabaseDAO.addWeather(weather)
    }

    fun getWeather(cId: Int): LiveData<WeatherAndCity> {
        return appDatabaseDAO.getWeather(cId)
    }

    fun getWeatherAndCity(): LiveData<List<WeatherAndCity>> {
        return appDatabaseDAO.getWeatherAndCity()
    }

    fun updateOrInsertWeather(weather: Weather) {
        val id = weather.wId
        val tmp = appDatabaseDAO.getWeatherFromId(id)
        if (tmp == null) {
            appDatabaseDAO.addWeather(weather)
        } else {
            appDatabaseDAO.updateWeather(weather)
        }
    }

    fun updateOrInsertCity(city: City) {
        val id = city.cId
        val tmp = appDatabaseDAO.getCityById(id)
        if (tmp == null) {
            appDatabaseDAO.addCity(city)
        } else {
            appDatabaseDAO.updateCity(city)
        }
    }

}