/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.TypeConverters
import com.simpleweather.database.Converters
import com.simpleweather.response.Current
import com.simpleweather.response.Daily
import com.simpleweather.response.Hourly
import java.io.Serializable


@Entity(tableName = "Weather", foreignKeys = [ForeignKey(entity = City::class,
        parentColumns = ["c_id"],
        childColumns = ["w_id"],
        onDelete = ForeignKey.CASCADE)], primaryKeys = ["w_id"])
@TypeConverters(Converters::class)
data class Weather(
        @ColumnInfo(name = "timezone_offset") var timezoneOffset: Int,
        @ColumnInfo(name = "current") var current: Current,
        @ColumnInfo(name = "hourly") var hourly: MutableList<Hourly>,
        @ColumnInfo(name = "daily") var daily: MutableList<Daily>,
        @ColumnInfo(name = "w_id") var wId: Int,
) : Serializable
