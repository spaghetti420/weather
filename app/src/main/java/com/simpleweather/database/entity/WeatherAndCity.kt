/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.TypeConverters
import com.simpleweather.database.Converters
import com.simpleweather.response.Current
import com.simpleweather.response.Daily
import com.simpleweather.response.Hourly

@Entity
@TypeConverters(Converters::class)
data class WeatherAndCity(
        var timezone_offset: Int,
        var current: Current,
        var hourly: MutableList<Hourly>? = null,
        var daily: MutableList<Daily>? = null,
        @ColumnInfo(name = "w_id")
        var wId: Int,
        var lat: Float,
        var lon: Float,
        //@ColumnInfo(name = "w_id")
        // var cId : Int,
        var country: String,
        var name: String,
)
