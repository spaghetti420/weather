/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.model

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.simpleweather.BuildConfig.DEBUG
import com.simpleweather.WeatherDataService
import com.simpleweather.database.entity.City
import com.simpleweather.database.entity.Weather
import com.simpleweather.database.entity.WeatherAndCity
import com.simpleweather.database.repository.AppDatabaseRepository
import com.simpleweather.response.OneCallResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class AppDatabaseViewModel(application: Application) : AndroidViewModel(application), WeatherDataService.WeatherDataListener {

    val TAG = javaClass.toString()

    private val appDatabaseRepository: AppDatabaseRepository = AppDatabaseRepository(application)
    lateinit var weatherAndCity: LiveData<List<WeatherAndCity>>
    private var weatherDataService: WeatherDataService = WeatherDataService(application.applicationContext)

    init {
        weatherDataService.addListener(this)
        viewModelScope.launch {
            weatherAndCity = appDatabaseRepository.getWeatherAndCity()
        }
    }

    fun addSavedCity(city: City) {
        viewModelScope.launch {
            withContext(viewModelScope.coroutineContext + Dispatchers.IO) {
                appDatabaseRepository.updateOrInsertCity(city)
                weatherDataService.updateOneCallWeatherData(city)
            }
        }
    }

    fun getWeatherDataService(): WeatherDataService {
        return weatherDataService
    }

    fun updateAll() {
        viewModelScope.launch {
            withContext(viewModelScope.coroutineContext + Dispatchers.IO) {
                val cities = appDatabaseRepository.getAllSavedCities()
                for (city in cities) {
                    weatherDataService.updateOneCallWeatherData(city)
                }
            }
        }
    }

    fun removeSavedCity(cId: Int) {
        viewModelScope.launch {
            withContext(viewModelScope.coroutineContext + Dispatchers.IO) {
                appDatabaseRepository.removeSavedCity(cId)
            }
        }
    }


    private fun addWeather(weather: Weather) {
        viewModelScope.launch {
            withContext(viewModelScope.coroutineContext + Dispatchers.IO) {
                if (DEBUG) {
                    Log.d(TAG, "Updating weather for city with timezone offset ${weather.timezoneOffset}  current time ${weather.current.dt} size of daily forecast list ${weather.daily.size}")
                }
                appDatabaseRepository.updateOrInsertWeather(weather)
            }
        }
    }

    suspend fun getWeather(cId: Int): LiveData<WeatherAndCity> {
        var weather: LiveData<WeatherAndCity>
        withContext(Dispatchers.IO) {
            weather = appDatabaseRepository.getWeather(cId)
        }
        return weather
    }

    override fun onOneCallWeatherUpdateSuccess(oneCallResponse: OneCallResponse, cached: Boolean, city: City) {
        runBlocking {
            withContext(viewModelScope.coroutineContext + Dispatchers.IO) {
                val hourly = oneCallResponse.hourly
                val wId = city.cId
                val weather = Weather(oneCallResponse.timezoneOffset, oneCallResponse.current, hourly, oneCallResponse.daily, wId)
                addWeather(weather)
            }
        }
    }

    override fun onOneCallWeatherUpdateFailed(errorMessage: String) {
    }
}