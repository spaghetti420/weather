/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.model

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.*
import com.simpleweather.database.entity.City
import com.simpleweather.database.repository.CitiesListRepository
import kotlinx.coroutines.*

class CitiesListViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var citiesListRepository: CitiesListRepository
    private val searchStringLiveData = MutableLiveData<String>("")
    lateinit var citiesList: LiveData<List<City>>

    init {
        viewModelScope.launch {
            citiesListRepository = CitiesListRepository(application)
            citiesList = Transformations.switchMap(searchStringLiveData)
            { string ->
                if (TextUtils.isEmpty(string)) {
                    null
                } else {
                    citiesListRepository.searchByCityName(string)
                }
            }
        }
    }

    fun searchNameChanged(name: String) {
        viewModelScope.async {
            searchStringLiveData.value = name
        }
    }

    private suspend fun nearest(lat: Float, lon: Float): City? {
        var nearestCity: City?
        withContext(Dispatchers.IO) {
            nearestCity = citiesListRepository.getNearestCity(lat, lon)
        }
        return nearestCity
    }

    fun getNearestCity(lat: Float, lon: Float): City? {
        var result: City?
        runBlocking {
            result = nearest(lat, lon)
        }
        return result

    }
}