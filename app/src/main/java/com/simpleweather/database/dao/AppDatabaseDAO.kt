/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.simpleweather.database.entity.City
import com.simpleweather.database.entity.Weather
import com.simpleweather.database.entity.WeatherAndCity

@Dao
interface AppDatabaseDAO : CitiesListDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addCity(vararg city: City)

    @Update()
    fun updateCity(vararg city: City)

    @Query("DELETE FROM Cities WHERE c_id= :cId ")
    fun removeCity(cId: Int)

    @Query("SELECT * FROM Cities WHERE c_id= :cId ")
    fun getCityById(cId: Int): City?

    @Query("SELECT * FROM Weather")
    fun getAllWeather(): LiveData<List<Weather>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addWeather(vararg weather: Weather)

    @Update()
    fun updateWeather(vararg weather: Weather)

    @Query("SELECT * FROM Cities")
    fun getCities(): List<City>

    @Query("SELECT * FROM Weather WHERE w_id= :wId ")
    fun getWeatherFromId(wId: Int): Weather

    @Query("SELECT timezone_offset,current,hourly,daily,w_id,lat,lon,country,name FROM Weather w NATURAL JOIN Cities c WHERE c.c_id = w.w_id GROUP BY name,country ORDER BY c_id")
    fun getWeatherAndCity(): LiveData<List<WeatherAndCity>>

    @Query("SELECT timezone_offset,current,hourly,daily,w_id,lat,lon,country,name FROM Weather w NATURAL JOIN Cities c WHERE c.c_id = :cId and w.w_id = :cId")
    fun getWeather(cId: Int): LiveData<WeatherAndCity>
}