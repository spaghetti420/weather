/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.util

import android.util.Log
import android.view.View
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.SwipeRevealLayout
import com.simpleweather.R
import com.simpleweather.fragment.SavedCitiesViewAdapter
import com.simpleweather.fragment.SavedCitiesViewAdapter.Companion.SELECTION_CHANGED
import jp.wasabeef.recyclerview.animators.BaseItemAnimator


class SavedCityItemAnimator() : BaseItemAnimator() {

    class SavedCityItemHolderInfo(val wasSelected: Boolean) : ItemHolderInfo()

    override fun canReuseUpdatedViewHolder(
            viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>
    ): Boolean = true

    override fun canReuseUpdatedViewHolder(viewHolder: RecyclerView.ViewHolder) = true

    override fun recordPreLayoutInformation(
            state: RecyclerView.State,
            viewHolder: RecyclerView.ViewHolder,
            changeFlags: Int,
            payloads: MutableList<Any>
    ): ItemHolderInfo {

        if (changeFlags == FLAG_CHANGED) {
            for (payload in payloads) {
                if (payload as? Int == SELECTION_CHANGED) {
                    return SavedCityItemHolderInfo((viewHolder as SavedCitiesViewAdapter.ViewHolder).isSelected())
                }
            }
        }
        return super.recordPreLayoutInformation(state, viewHolder, changeFlags, payloads)
    }

    override fun animateChange(
            oldHolder: RecyclerView.ViewHolder, newHolder: RecyclerView.ViewHolder,
            preInfo: ItemHolderInfo, postInfo: ItemHolderInfo
    ): Boolean {

        val holder = newHolder as SavedCitiesViewAdapter.ViewHolder
        if (holder.isSelected()) {
            if (holder.itemView is SwipeRevealLayout) {
                holder.itemView.open(true)
            }
        } else {
            if (holder.itemView is SwipeRevealLayout) {
                holder.itemView.close(true)
            }
        }

        return super.animateChange(oldHolder, newHolder, preInfo, postInfo)
    }

    override fun getRemoveDuration(): Long {
        return 685
    }

    override fun getAddDuration(): Long {
        return 558
    }

    override fun animateRemoveImpl(holder: RecyclerView.ViewHolder) {
        val backgroundView: View? = holder.itemView.findViewById(R.id.background)
        val foregroundView: View? = holder.itemView.findViewById(R.id.foreground)
        val viewWidth = holder.itemView.width
        backgroundView?.let {
            it.animate().scaleX(0.75f).scaleY(0.75f)
                    .alpha(0f).setDuration(removeDuration).setInterpolator(FastOutSlowInInterpolator())
        }
        foregroundView?.let {
            it.animate().translationX(viewWidth * -1f).setDuration(removeDuration)
                    .setInterpolator(FastOutSlowInInterpolator())
        }
        holder.itemView.animate().apply {
            translationY(0f)
            duration = removeDuration
            interpolator = FastOutSlowInInterpolator()
            setListener(DefaultRemoveAnimatorListener(holder))
        }.start()
    }

    override fun preAnimateAddImpl(holder: RecyclerView.ViewHolder) {
        holder.itemView.scaleX = 0.75f
        holder.itemView.scaleY = 0.75f
        holder.itemView.alpha = 0f
    }

    override fun animateAddImpl(holder: RecyclerView.ViewHolder) {
        holder.itemView.animate().apply {
            scaleX(1f)
            scaleY(1f)
            alpha(1f)
            duration = addDuration
            interpolator = FastOutSlowInInterpolator()
            setListener(DefaultAddAnimatorListener(holder))
        }.start()
    }
}
