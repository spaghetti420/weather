/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.util

import android.content.Context
import androidx.preference.PreferenceManager
import com.simpleweather.R
import com.simpleweather.fragment.SettingsFragment
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.floor
import kotlin.math.roundToInt

class WeatherUtils(context: Context) {

    private val sharePrefs = PreferenceManager.getDefaultSharedPreferences(context)

    private val imperial = 2
    private val metric = 1
    private val default: Int
        get() = getUnits(Locale.getDefault())

    private fun getUnits(locale: Locale): Int {
        val measurementUnits = sharePrefs.getString(SettingsFragment.MEASUREMENT_UNITS_KEY, "0")
        val countryCode = locale.country
        if (measurementUnits == "1") return metric
        if (measurementUnits == "2") return imperial
        if ("US" == countryCode) return imperial // USA
        if ("LR" == countryCode) return imperial // Liberia
        return if ("MM" == countryCode) imperial else metric // Myanmar
    }

    private fun kelvinToCelsius(degrees: Float): Float {
        return degrees - 273.15f
    }

    private fun kelvinToFahrenheit(degrees: Float): Float {
        return (degrees - 273.15f) * 1.8f + 32.0f
    }

    fun getFormattedTemperature(degrees: Float) : String {
        return if (default == imperial) {
            "${kelvinToFahrenheit(degrees).roundToInt()} °F"
        } else {
            "${kelvinToCelsius(degrees).roundToInt()} °C"
        }
    }

    fun getDateFromEpoch(epoch: Long, shiftSeconds: Int) : String {
        return formatDate(epoch, shiftSeconds, "yyyyMMdd")
    }

    fun getTimeFromEpoch(epoch: Long, shiftSeconds: Int) : String {
        return formatDate(epoch, shiftSeconds, "HH:mm")
    }

    fun getWeekDayFromEpoch(epoch: Long, shiftSeconds: Int) : String {
        return formatDate(epoch, shiftSeconds, "EEEE")
    }

    private fun formatDate(epoch: Long, shiftSeconds: Int, pattern: String): String {
        val sdf = SimpleDateFormat(pattern)
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val time = Date((epoch + shiftSeconds) * 1000)
        return sdf.format(time)
    }

    fun getWindDirectionString(degrees: Int) : String {
        val value = floor((degrees / 22.5) + 0.5)
        return directions[(value % 16).roundToInt()]
    }

    private fun convertMetersToMiles(meters: Float): Float {
        return meters * 0.000621371f
    }

    private fun truncateFloatToString(number: Float, decimals: Int): String {
        val shouldTruncate = ((number - number.toInt()) * 10).roundToInt() == 0
        return if (shouldTruncate) "${number.toInt()}" else "${"%.${decimals}f".format(number)}"
    }

    fun getFormattedDistance(meters: Float) : String {
        val distance = if (default == imperial) convertMetersToMiles(meters) else meters / 1000
        val unit = if (default == imperial) "miles" else "km"
        return "${truncateFloatToString(distance, 1)} $unit"
    }

    private fun metersPerSecondsToMilesPerHour(metersPerSecond: Float): Float {
        return metersPerSecond * 2.2369362920544f
    }

    fun getFormattedSpeed(metersPerSecond : Float) : String {
        return if (default == imperial) {
            "${(metersPerSecondsToMilesPerHour(metersPerSecond)).roundToInt()} mph"
        } else {
            "${(metersPerSecond).roundToInt()} m/s"
        }
    }

    fun getFeelsLikeFormattedTemp(context : Context, temperature : Float) : String {
        return context.getString(R.string.feels_like) + " " + getFormattedTemperature(temperature)
    }

    fun getBackgroundResId(conditionCode: Int): Int {
        return when (conditionCode) {
            in 200..299 -> R.drawable.thunderstorm
            in 300..399 -> R.drawable.drizzle
            in 500..599 -> R.drawable.rain
            in 600..699 -> R.drawable.snow
            in 700..799 -> R.drawable.fog
            in 802..899 -> R.drawable.cloudy
            else -> R.drawable.clear
        }
    }

    companion object {
        const val API_KEY = "19063415dbe8507f4bd3e92ad691a57e"
        const val OPENWEATHER_URL = "https://api.openweathermap.org/"

        val directions = arrayOf("N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW")
    }
}
