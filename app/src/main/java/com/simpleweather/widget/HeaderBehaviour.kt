/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.zhenxiang.resourcesviewer.packageselector

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.simpleweather.R

class HeaderBehaviour(
    private val mContext: Context,
    attrs: AttributeSet?
) : CoordinatorLayout.Behavior<Toolbar>(mContext, attrs) {
    private var mStartMarginBottom = 0
    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: Toolbar,
        dependency: View
    ): Boolean {
        return dependency is AppBarLayout
    }

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: Toolbar,
        dependency: View
    ): Boolean {
        shouldInitProperties()
        val maxScroll = (dependency as AppBarLayout).totalScrollRange
        val percentage =
            Math.abs(dependency.getY()) / maxScroll.toFloat()
        var childPosition = ((dependency.getHeight()
                + dependency.getY()) - child.height
                - (getToolbarHeight(mContext) - child.height) * percentage / 2)
        childPosition = childPosition - mStartMarginBottom * (1f - percentage)
        child.y = childPosition
        return true
    }

    private fun getTranslationOffset(
        expandedOffset: Float,
        collapsedOffset: Float,
        ratio: Float
    ): Float {
        return expandedOffset + ratio * (collapsedOffset - expandedOffset)
    }

    private fun shouldInitProperties() {
        if (mStartMarginBottom == 0) {
            mStartMarginBottom = mContext.resources
                .getDimensionPixelOffset(R.dimen.header_view_start_margin_bottom)
        }
    }

    companion object {
        fun getToolbarHeight(context: Context): Int {
            var result = 0
            val tv = TypedValue()
            if (context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                result = TypedValue.complexToDimensionPixelSize(
                    tv.data,
                    context.resources.displayMetrics
                )
            }
            return result
        }
    }

}