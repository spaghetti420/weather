/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather

import android.content.Context
import com.simpleweather.BuildConfig.DEBUG
import com.simpleweather.database.entity.City
import com.simpleweather.response.OneCallResponse
import com.simpleweather.util.NetworkConnectionInterceptor
import com.simpleweather.util.WeatherUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import javax.security.cert.CertificateException
import kotlin.jvm.Throws

class WeatherDataService(context: Context) {
    val TAG = this.javaClass.toString()

    private val okHttpClient: OkHttpClient
    private val retrofit: Retrofit
    private val service: WeatherService

    private val listeners: ArrayList<WeatherDataListener> = arrayListOf()

    // Must pass applicationContext as context
    init {
        val okHttpClientBuilder = OkHttpClient.Builder()
        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return arrayOf()
            }
        })

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory

        okHttpClientBuilder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        // builder.hostnameVerifier { _, _ -> true }
        okHttpClientBuilder.hostnameVerifier { _, _ -> true }
        if (DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = (HttpLoggingInterceptor.Level.BASIC)
            okHttpClientBuilder.addInterceptor(logging)
        }
        okHttpClientBuilder.addInterceptor(NetworkConnectionInterceptor(context))
        okHttpClient = okHttpClientBuilder.connectTimeout(15, TimeUnit.SECONDS).build()
        retrofit = Retrofit.Builder()
                .baseUrl(WeatherUtils.OPENWEATHER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
        service = retrofit.create(WeatherService::class.java)
    }

    fun addListener(listener: WeatherDataListener) {
        listeners.add(listener)
    }

    fun removeListener(listenerToRemove: WeatherDataListener) {
        for (listener in listeners) {
            if (listenerToRemove == listener) {
                listeners.remove(listener)
                break
            }
        }
    }

    fun updateOneCallWeatherData(city: City) {
        val callForecast = service.getOneCallData(city.lat.toString(), city.lon.toString(), WeatherUtils.API_KEY)
        callForecast.enqueue(object : Callback<OneCallResponse?> {
            override fun onResponse(call: Call<OneCallResponse?>, response: Response<OneCallResponse?>) {
                if (response.code() == 200) {
                    for (listener in listeners) {
                        listener.onOneCallWeatherUpdateSuccess(response.body()!!, response.raw().networkResponse == null, city)
                    }
                } else {
                    for (listener in listeners) {
                        listener.onOneCallWeatherUpdateFailed("Response code ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<OneCallResponse?>, t: Throwable) {
                for (listener in listeners) {
                    listener.onOneCallWeatherUpdateFailed(t.toString())
                }
            }
        })
    }

    interface WeatherDataListener {
        fun onOneCallWeatherUpdateSuccess(oneCallResponse: OneCallResponse, cached: Boolean, city: City)
        fun onOneCallWeatherUpdateFailed(errorMessage: String)
    }
}