/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.ColorUtils
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.revengeos.revengeui.utils.BitmapUtils
import com.revengeos.revengeui.utils.BounceEdgeEffect
import com.revengeos.revengeui.utils.ReflectionUtils
import com.simpleweather.BuildConfig.DEBUG
import com.simpleweather.fragment.CitiesFragment
import com.simpleweather.fragment.DayWeatherFragment
import com.simpleweather.fragment.SettingsFragment
import com.simpleweather.widget.DisableScrollingViewPager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.FileNotFoundException
import java.lang.Exception
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity(), DayWeatherFragment.DayWeatherFragmentListener,
        CitiesFragment.CityItemSelectedListener, SettingsFragment.CustomImageListener {

    val TAG = javaClass.toString()
    private val CACHED_BG_IMAGE_FILENAME = "cached_bg"

    private lateinit var bottomNav: BottomNavigationView

    private lateinit var pageBackground: ImageView
    private lateinit var pageBackgroundScrim: View
    private lateinit var pageBackgroundBlurScrim: View
    private var displayWidth = -1
    private var displayHeight = -1
    private val TARGET_BACKGROUND_BRIGHTNESS = 65

    private lateinit var fragmentsPager: DisableScrollingViewPager
    private lateinit var prevMenuItem: MenuItem
    private lateinit var homePagerAdapter: HomePagerAdapter
    private val pageChangeListener = object : ViewPager.SimpleOnPageChangeListener() {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            updateBackgroundTransition(position, positionOffset)
            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
        }

        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            prevMenuItem.isChecked = false
            bottomNav.menu.getItem(position).isChecked = true
            prevMenuItem = bottomNav.menu.getItem(position)
        }
    }

    private lateinit var topInsetScrimView: View

    private var weatherBitmapResId = 0
    private var useCustomImage = false
    private lateinit var sharedPreferences: SharedPreferences
    private val prefsListener = object : SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            if (key == SettingsFragment.APP_BACKGROUND_SETTING_KEY) {
                updateCustomImageSetting()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        WindowCompat.setDecorFitsSystemWindows(window, false)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPreferences.registerOnSharedPreferenceChangeListener(prefsListener)

        // Setup background and blur effects
        pageBackgroundBlurScrim = findViewById(R.id.blur_scrim)
        pageBackground = findViewById(R.id.page_background)
        pageBackgroundScrim = findViewById(R.id.page_background_scrim)
        val display: Display = (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        val size = Point()
        display.getSize(size)
        displayWidth = size.x
        displayHeight = size.y

        updateCustomImageSetting()

        // Setup fragments
        homePagerAdapter = HomePagerAdapter(supportFragmentManager)
        fragmentsPager = findViewById(R.id.fragments_pager)
        fragmentsPager.offscreenPageLimit = 3
        fragmentsPager.adapter = homePagerAdapter
        fragmentsPager.addOnPageChangeListener(pageChangeListener)
        ViewCompat.setOnApplyWindowInsetsListener(fragmentsPager, null)

        ReflectionUtils.getField(ViewPager::class.java.name, "mLeftEdge").set(fragmentsPager, BounceEdgeEffect(this, fragmentsPager, RecyclerView.EdgeEffectFactory.DIRECTION_LEFT))
        ReflectionUtils.getField(ViewPager::class.java.name, "mRightEdge").set(fragmentsPager, BounceEdgeEffect(this, fragmentsPager, RecyclerView.EdgeEffectFactory.DIRECTION_RIGHT))

        bottomNav = findViewById(R.id.main_nav)
        prevMenuItem = bottomNav.menu.getItem(0)
        ViewCompat.setOnApplyWindowInsetsListener(bottomNav, null)
        bottomNav.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.home_page -> {
                    fragmentsPager.currentItem = 0
                    true
                }
                R.id.cities -> {
                    fragmentsPager.currentItem = 1
                    true
                }
                R.id.nav_settings -> {
                    fragmentsPager.currentItem = 2
                    true
                }
                else -> false
            }
        }

        topInsetScrimView = findViewById(R.id.top_inset_scrim)

        ViewCompat.setOnApplyWindowInsetsListener(window.decorView) { _, inset ->
            val topInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).top
            topInsetScrimView.layoutParams.height = topInset

            val leftInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).left +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).left
            val rightInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).right +
                    WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.displayCutout()).right
            val bottomInset = WindowInsetsCompat(inset).getInsets(WindowInsetsCompat.Type.systemBars()).bottom
            val bottomNavMargins = bottomNav.layoutParams as ViewGroup.MarginLayoutParams
            bottomNavMargins.bottomMargin = bottomInset
            bottomNavMargins.leftMargin = leftInset
            bottomNavMargins.rightMargin = rightInset

            return@setOnApplyWindowInsetsListener inset
        }
    }

    private fun updateCustomImageSetting() {
        useCustomImage = sharedPreferences.getString(SettingsFragment.APP_BACKGROUND_SETTING_KEY, "0") == "1"
        if (!useCustomImage) {
            if (weatherBitmapResId != 0) {
                lifecycleScope.launch(Dispatchers.Default) {
                    val bitmap = BitmapUtils.decodeSampledBitmapFromResource(
                            pageBackground.context.resources, weatherBitmapResId, displayWidth, displayHeight)
                    setBackgroundImage(bitmap)
                }
            }
        } else {
            lifecycleScope.launch(Dispatchers.IO) {
                try {
                    val inputStream = openFileInput(CACHED_BG_IMAGE_FILENAME)
                    val bitmap = BitmapFactory.decodeStream(inputStream)
                    inputStream.close()
                    withContext(Dispatchers.Main) {
                        setBackgroundImage(bitmap)
                    }
                } catch (e: Exception) {
                    // Failed to open image, fallback to default setting
                    sharedPreferences.edit().putString(SettingsFragment.APP_BACKGROUND_SETTING_KEY, "0").commit()
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("selectedTab", bottomNav.selectedItemId)
        outState.putInt("selectedPage", fragmentsPager.currentItem)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        bottomNav.selectedItemId = savedInstanceState.getInt("selectedTab")
        updateBackgroundTransition(savedInstanceState.getInt("selectedPage"), 0f)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onDestroy() {
        // Avoid memory leaks
        fragmentsPager.adapter = null
        fragmentsPager.removeOnPageChangeListener(pageChangeListener)
        super.onDestroy()
    }

    override fun onBackgroundChanged(drawableId: Int) {
        if ((displayHeight > 0 && displayWidth > 0)) {
            lifecycleScope.launch(Dispatchers.Default) {
                val bitmap = BitmapUtils.decodeSampledBitmapFromResource(
                        pageBackground.context.resources, drawableId, displayWidth, displayHeight)
                weatherBitmapResId = drawableId
                if (!useCustomImage) {
                    setBackgroundImage(bitmap)
                }
            }
        }
    }

    override fun goToCitiesFragment() {
        fragmentsPager.currentItem = 1
    }

    private fun setBackgroundImage(bitmap: Bitmap) {
        lifecycleScope.launch(Dispatchers.Default) {
            val bitmapBrightness = BitmapUtils.calculateBrightnessEstimate(bitmap, 1)[0]
            val scrimAlpha = Math.max(bitmapBrightness - TARGET_BACKGROUND_BRIGHTNESS, 0f).roundToInt()
            val scrimColour = ColorUtils.setAlphaComponent(Color.BLACK, scrimAlpha)
            withContext(Dispatchers.Main) {
                pageBackground.setImageBitmap(bitmap)
                pageBackgroundScrim.setBackgroundColor(scrimColour)
            }
        }
    }

    private fun updateBackgroundTransition(position: Int, positionOffset: Float) {
        // Poorly implemented, I'll improve the logic in future
        if (position == 0) {
            if (positionOffset > 0) {
                if (pageBackgroundBlurScrim.visibility == View.GONE) {
                    pageBackgroundBlurScrim.visibility = View.VISIBLE
                }
                if (pageBackgroundScrim.visibility == View.GONE) {
                    pageBackgroundScrim.visibility = View.VISIBLE
                    topInsetScrimView.visibility = View.VISIBLE
                }
                pageBackgroundBlurScrim.alpha = positionOffset
                pageBackgroundScrim.alpha = 1 - positionOffset
                topInsetScrimView.alpha = 1 - positionOffset
            } else {
                pageBackgroundBlurScrim.visibility = View.GONE
                pageBackgroundBlurScrim.alpha = 0f
                pageBackgroundScrim.alpha = 1f
                topInsetScrimView.alpha = 1f

            }
        } else {
            if (pageBackgroundBlurScrim.visibility == View.GONE) {
                pageBackgroundBlurScrim.visibility = View.VISIBLE
            }
            if (pageBackgroundBlurScrim.alpha != 1f) {
                pageBackgroundBlurScrim.alpha = 1f
            }
            if (pageBackgroundScrim.visibility == View.VISIBLE) {
                pageBackgroundScrim.visibility = View.GONE
                pageBackgroundScrim.alpha = 0f
                topInsetScrimView.visibility = View.GONE
                topInsetScrimView.alpha = 0f
            }
        }
    }

    private class HomePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> DayWeatherFragment.newInstance()
                1-> CitiesFragment.newInstance()
                2-> SettingsFragment.newInstance()
                else -> throw Exception()
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }

    override fun onCitySelected(wId: Int) {
        if(DEBUG) {
            Log.d(TAG, "Selected city $wId")
        }
        fragmentsPager.currentItem = 0
        Handler(Looper.getMainLooper()).postDelayed({
            val fragment = homePagerAdapter.instantiateItem(fragmentsPager, 0)
            if (fragment is DayWeatherFragment) {
                fragment.changeWIdToListen(wId)
            }
        }, 150)
    }

    override fun onCityDeleted(wId: Int) {
        if(DEBUG) {
            Log.d(TAG, "Delete selected city $wId")
        }
    }

    override fun onCustomImageSelected(uri: Uri) {
        if (displayHeight > 0 && displayWidth > 0) {
            lifecycleScope.launch(Dispatchers.IO) {
                // First decode with inJustDecodeBounds=true to check dimensions
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                try {
                    BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, options)

                    // Calculate inSampleSize
                    options.inSampleSize = BitmapUtils.calculateInSampleSize(options, displayWidth, displayHeight)

                    // Decode bitmap with inSampleSize set
                    options.inJustDecodeBounds = false
                    val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, options)
                    if (bitmap != null) {
                        setBackgroundImage(bitmap)
                        val outputStream = openFileOutput(CACHED_BG_IMAGE_FILENAME, Context.MODE_PRIVATE)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                    }
                } catch (e: FileNotFoundException) {
                    // File can't be found anymore
                }
            }
        }
    }
}