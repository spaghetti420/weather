/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather.credits

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.simpleweather.R

class CreditsDialogFragment: DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireContext()).create()

        val creditsLayout = dialog.layoutInflater.inflate(R.layout.credits_dialog_fragment, null)
        val developersList = requireContext().resources.getStringArray(R.array.developers_list).toList()
        val developersRecyclerView = creditsLayout.findViewById<RecyclerView>(R.id.developers_list)
        developersRecyclerView.adapter = CreditsPeopleAdapter(developersList)
        developersRecyclerView.layoutManager = LinearLayoutManager(developersRecyclerView.context)

        val translatorsList = requireContext().resources.getStringArray(R.array.translators_list).toList()
        val translatorsRecyclerView = creditsLayout.findViewById<RecyclerView>(R.id.translators_list)
        translatorsRecyclerView.adapter = CreditsPeopleAdapter(translatorsList)
        translatorsRecyclerView.layoutManager = LinearLayoutManager(translatorsRecyclerView.context)
        dialog.setView(creditsLayout)
        return dialog
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                CreditsDialogFragment().apply {
                }
    }
}