/*
 * Simple Weather
 * Copyright (c) 2021 Zhenxiang Chen & Ethan Halsall
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.simpleweather

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayout
import com.simpleweather.util.WeatherUtils
import kotlin.math.roundToInt

class WeatherDataGridView : FlexboxLayout {

    private val weatherUtils: WeatherUtils

    private val sunriseView: WeatherGridItemView
    private val sunsetView: WeatherGridItemView
    private val pressureView: WeatherGridItemView
    private val humidityView: WeatherGridItemView
    private val windView: WeatherGridItemView
    private val visibilityDistanceView : WeatherGridItemView
    private val precipitationsView : WeatherGridItemView
    private val minTempView: WeatherGridItemView
    private val maxTempView: WeatherGridItemView
    private val feelsLikeTempView: WeatherGridItemView
    private val uvView: WeatherGridItemView
    private val dewPointView: WeatherGridItemView

    constructor (context: Context) : this(context, null) {
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {

        weatherUtils = WeatherUtils(context)

        flexWrap = FlexWrap.WRAP
        LayoutInflater.from(context).inflate(R.layout.weather_data_grid, this, true)

        sunriseView = findViewById(R.id.current_sunrise)
        sunsetView = findViewById(R.id.current_sunset)
        pressureView = findViewById(R.id.current_pressure)
        humidityView = findViewById(R.id.current_humidity)
        windView = findViewById(R.id.current_wind)
        visibilityDistanceView = findViewById(R.id.current_visibility)
        precipitationsView = findViewById(R.id.current_precipitations)
        minTempView = findViewById(R.id.current_min_temp)
        maxTempView = findViewById(R.id.current_max_temp)
        feelsLikeTempView = findViewById(R.id.current_feels_like_temp)
        uvView = findViewById(R.id.current_uv)
        dewPointView = findViewById(R.id.current_dew_point)

    }

    fun updateData(weatherGridData : WeatherGridData) {

        if (weatherGridData.minTemp == null) {
            minTempView.visibility = GONE
        } else {
            minTempView.valueView.text = weatherUtils.getFormattedTemperature(weatherGridData.minTemp)
            minTempView.visibility = VISIBLE
        }
        if (weatherGridData.maxTemp == null) {
            maxTempView.visibility = GONE
        } else {
            maxTempView.valueView.text = weatherUtils.getFormattedTemperature(weatherGridData.maxTemp)
            maxTempView.visibility = VISIBLE
        }

        if (weatherGridData.sunrise == null) {
            sunriseView.visibility = GONE
        } else {
            sunriseView.valueView.text = weatherUtils.getTimeFromEpoch(weatherGridData.sunrise, weatherGridData.timeZone)
            sunriseView.visibility = VISIBLE
        }
        if (weatherGridData.sunset == null) {
            sunsetView.visibility = GONE
        } else {
            sunsetView.valueView.text = weatherUtils.getTimeFromEpoch(weatherGridData.sunset, weatherGridData.timeZone)
            sunsetView.visibility = VISIBLE
        }
        pressureView.valueView.text = "${weatherGridData.pressure} hPa"
        humidityView.valueView.text = "${weatherGridData.humidity} %"
        if (weatherGridData.windSpeed == null || weatherGridData.windDirection == null) {
            windView.visibility = GONE
        } else {
            windView.visibility = View.VISIBLE
            windView.valueView.text = "${weatherUtils.getFormattedSpeed(weatherGridData.windSpeed)} ${weatherUtils.getWindDirectionString(weatherGridData.windDirection)}"
        }
        if (weatherGridData.visibility == null) {
            visibilityDistanceView.visibility = GONE
        } else {
            visibilityDistanceView.valueView.text = weatherUtils.getFormattedDistance(weatherGridData.visibility.toFloat())
            visibilityDistanceView.visibility = VISIBLE
        }
        if (weatherGridData.precipitations == null) {
            precipitationsView.visibility = GONE
        } else {
            precipitationsView.valueView.text = "${(weatherGridData.precipitations * 100).roundToInt()} %"
            precipitationsView.visibility = View.VISIBLE
        }
        if (weatherGridData.uv == null) {
            uvView.visibility = GONE
        } else {
            uvView.valueView.text = "${weatherGridData.uv}"
            uvView.visibility = VISIBLE
        }
        if (weatherGridData.feelsLikeTemp == null) {
            feelsLikeTempView.visibility = GONE
        } else {
            feelsLikeTempView.valueView.text = weatherUtils.getFormattedTemperature(weatherGridData.feelsLikeTemp)
            feelsLikeTempView.visibility = VISIBLE
        }
        if (weatherGridData.dewPoint == null) {
            dewPointView.visibility = GONE
        } else {
            dewPointView.valueView.text = weatherUtils.getFormattedTemperature(weatherGridData.dewPoint)
            dewPointView.visibility = VISIBLE
        }
    }
}